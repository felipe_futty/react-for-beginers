import React from "react";
import {formatPrice} from '../helpers';


class Fish extends React.Component {

  render() {
     const name = this.props.fish.name;
     const price = this.props.fish.price;
     const img =  this.props.fish.image;
     const description = this.props.fish.desc;
     const available = this.props.fish.status === 'available';

     return (
         <li className="menu-fish">
             <img src={img} alt={name} />
             <h3 className="fish-name">
               {name}
               <span className="price">{formatPrice(price)}</span>
             </h3>
             <p>{description}</p>
             <button 
                  disabled={!available}
                  onClick={() => {this.props.addOrder(this.props.index)}}
             >
                       {available ? 'Add to Cart' : 'Sold out'}
             </button>
         </li>
     );
  }
}

export default Fish;
