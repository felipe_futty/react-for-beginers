import React from "react";
import Header from './Header';
import Inventory from './Inventory';
import Order from './Order';
import sampleFishes from '../sample-fishes';
import Fish from './Fish';
import base from '../base';

class App extends React.Component {
    state =  {
        fishes: {},
        order: {}
    };

    /*** Custom Methos ***/

    updateFish = (key, newFish) => {
        // 1. Make a copy of current state
        const copyFish = { ...this.state.fishes };

        // 2. Update the copy
        copyFish[key] = newFish;

       // 3. Set state
       this.setState({fishes: copyFish});
    }

    addFish = (fish) => {
       // 1. Make a copy
       const copyFish = { ...this.state.fishes };

       // 2. Add fish with unique id
       copyFish[`fish${Date.now()}`] = fish;

       // 3. Update using setState (OBLIGATION)
       this.setState({
           fishes: copyFish
       });
    }

    loadSampleFishes = () => {
      this.setState({fishes: sampleFishes}); 
    }

    addOrder = (key) => {
       //1. Make a copy
       const copyOrder = { ...this.state.order };
       
       //2. Add a order
       copyOrder[key] = copyOrder[key] + 1 || 1;

       //3. Update order state using setState (OBLIGATION)
       this.setState({order: copyOrder});
    }

    removeFromOrder = (key) => {
       //1. Make a copy
       const copyOrder = { ...this.state.order };
       
       //2. delete a order
       delete copyOrder[key];

       //3. Update order state using setState (OBLIGATION)
       this.setState({order: copyOrder});
    }

    stateArrayToJSX = () => {

       // NOTE: key is a html requirement to <li> objects (IT IS NOT A PROPS)
       // to acces the variable passa props as "index"
       return Object.keys(this.state.fishes).map(
                (key) => <Fish fish={this.state.fishes[key]} key={key} index={key} addOrder={this.addOrder} />
              );
    }

    /**** Inhentencie methods ****/

    render() {
        return (
        <div className="catch-of-the-day">
            <div className="menu">
                <Header tagline="Fresh Seafood Market"/>
                <ul className="fishes">
			{ this.stateArrayToJSX() }
                </ul>               
            </div>

            <Order fishes={this.state.fishes} order={this.state.order} removeFromOrder={this.removeFromOrder} /> 
            <Inventory addFish={this.addFish} loadSampleFishes={this.loadSampleFishes} 
                 fishes={this.state.fishes} updateFish={this.updateFish}
             />
        </div>
        );
    }

    componentDidMount() {
        // Sync inventory with cloud
        const { params } = this.props.match;
        this.ref = base.syncState(`${params.storeId}/fishes`, {
            context: this,
            state: 'fishes'
        });

        // Sync order locally (sync fishes is async, so it couldn't exist fishes yet
        const localStorageRef = localStorage.getItem(params.storeId);
        if (localStorageRef) {
            this.setState({order: JSON.parse(localStorageRef)});
        }
    }

    componentWillUnmount() {
        base.removeBinding(this.ref);
    }

    componentDidUpdate() {
        localStorage.setItem(
            this.props.match.params.storeId,
            JSON.stringify(this.state.order)
        );
    }
}

export default App;
