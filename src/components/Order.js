import React from 'react';
import { formatPrice } from '../helpers';

class Header extends React.Component {
    showOrder = (key) => {
        const fish = this.props.fishes[key];

        // ONLY CONTINUE WHEN EXIST FISHES!!
        if (!fish) return null;

        const isAvailable = fish && fish.status === 'available';
        const quantity = this.props.order[key];

        if (!isAvailable) {
            return <li key={key} > {fish ? fish.name : 'Fish'} is not available </li>
        }
        return (
            <li key={key} >
                {quantity} lbs {fish.name}
                {formatPrice(fish.price * quantity)} 
                <button onClick={() => this.props.removeFromOrder(key)}>&times;</button>
            </li> 
        );
    }

    render() {
        const orderIds = Object.keys(this.props.order);
        const total = orderIds.reduce((count, key) => {
            const fish = this.props.fishes[key];


            const isAvailable = fish && fish.status === 'available';
            if (isAvailable) { 
                return count + (this.props.order[key] * fish.price);
            }
            return count;
        }, 0);

        return (
            <div className="order-wrap">
                <h2> Order </h2>
                <ul className="order"> 
                  {orderIds.map(this.showOrder)}
                </ul>
                <div className="total">
                    Total:
                     <strong>{formatPrice(total)}</strong>
                </div>
            
            </div>
        );
    }
}

export default Header;
