import React from 'react';
import { getFunName } from '../helpers';

class StorePicker extends React.Component {
    // Create a prop that sabe the input (reference it without jquery)
    myInput = React.createRef();

    // custom function to handle event (use fat arrow for having this access)
    goToStore = (event) => {
	 console.log("1");
        // 1. Stop form from submit
        event.preventDefault();

        // 2. get the input text from form (two value to access it)
        const storeName = this.myInput.current.value;

        // 3. change to store page (using router)
        // given that the router is parent, it has acces to it
        // using notation to creating string with js vars
	this.props.history.push(`/store/${storeName}`);
    }

    // method from super class (already bind the "this")
    render() {
        return (
            <form className="store-selector" onSubmit={this.goToStore} >
                <h2>Please enter a store name</h2>
                <input 
                    type="text" 
                    ref={this.myInput}
                    required 
                    placeholder="Store Name"
                    defaultValue={getFunName()} 
                />
                <button type="submit" >Visit Store -></button>	
            </form>
	); 
    }
}

export default StorePicker;

