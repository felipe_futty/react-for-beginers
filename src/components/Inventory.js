import React from 'react';
import AddFishForm from "./AddFishForm";
import EditFishForm from './EditFishForm';


class Inventory extends React.Component {
    render() {
        return (
            <div className="inventory" >
              <h2>Inventory</h2>
              <h3> Current </h3>
              {Object.keys(this.props.fishes).map( (key) => <EditFishForm key={key} fish={this.props.fishes[key]} index={key} updateFish={this.props.updateFish} />)}
              <h3> Add Fish </h3>
              <AddFishForm addFish={this.props.addFish} />
              <button onClick={this.props.loadSampleFishes}>Load Sample Fishes!</button>
            </div>
        );
    }
}

export default Inventory;
