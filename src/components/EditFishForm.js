import React from "react";

class EditFishForm extends React.Component {
    handleChange = (event) => {
        // 1. Take a copy of current fish (NOTE ADVANCED COPY)
        const updatedFish = {
              ...this.props.fish,
              [event.currentTarget.name]: event.currentTarget.value
        };

        // 2. Call up method to update state
        this.props.updateFish(this.props.index, updatedFish);
    }

    render() {
      return (
        <form className="fish-edit">
           <input name="name" onChange={this.handleChange} type="text" value={this.props.fish.name} />
           <input name="price" onChange={this.handleChange}  type="text" value={this.props.fish.price} />
           <select name="status" onChange={this.handleChange} value={this.props.fish.status}>
               <option value="available">Fresh Fish!</option>
               <option value="unavailable">Sold out!</option>
           </select>
           <textarea name="desc" onChange={this.handleChange} value={this.props.fish.desc} />
           <input name="image" onChange={this.handleChange} type="text" value={this.props.fish.image} />
           <button onClick={() => {this.props.updateFish(this.props.index, null)} }>Delete Fish</button>
        </form>
      );
    }

}

export default EditFishForm;
