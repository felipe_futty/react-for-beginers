import Rebase from 're-base';
import firebase from 'firebase';

// Initialize Firebase
var config = {
    apiKey: "AIzaSyDpwzA_eTXPY8MSOozwzxCNIH2SxQnJcoI",
    authDomain: "catch-of-the-day-134a3.firebaseapp.com",
    databaseURL: "https://catch-of-the-day-134a3.firebaseio.com"
};
const firebaseApp = firebase.initializeApp(config);

const base = Rebase.createClass(firebaseApp.database());

// Export as name
export { firebaseApp };

// Export as module default
export default base;
